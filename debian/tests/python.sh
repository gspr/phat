#!/bin/bash

set -e

for py3ver in $(py3versions -vs)
do
    echo "Running tests with Python ${py3ver}."
    /usr/bin/python${py3ver} -B debian/tests/python.py
    echo "---------"
done

