import phat

def main():
    files = ["single_triangle"]

    for file in files:
        ref = []
        with open("debian/tests/reference/%s.txt" %(file), "r") as f:
            f.readline()
            for line in f:
                splitline = line.strip().split(" ")
                assert(len(splitline) == 2)
                ref.append((int(splitline[0]), int(splitline[1])))
        ref.sort()

        cols = []
        with open("examples/%s.dat" %(file), "r") as f:
            for line in f:
                if not line.startswith("#") and line.strip() != "":
                    splitline = line.rstrip().split(" ")
                    cols.append((int(splitline[0]), [int(x) for x in splitline[1:]]))

        for repr in phat.representations:
            for alg in phat.reductions:
                for dualize in [False, True]:
                    print("File %s, representation %s, algorithm %s, dualization %s" %(file, repr, alg, str(dualize)))
                
                    bd = phat.boundary_matrix(representation=repr)
                    bd.columns = cols
                    if dualize:
                        pairs = list(bd.compute_persistence_pairs_dualized(reduction=alg))
                    else:
                        pairs = list(bd.compute_persistence_pairs(reduction=alg))
                    pairs.sort()

                    if ref == pairs:
                        print("OK!")
                    else:
                        print("ERROR!")
                        print("Got", pairs)
                        print("Expected", ref)
                        exit(1)
                

if __name__ == "__main__":
    main()
