#!/bin/bash

files="single_triangle"
reprs="vector_vector vector_heap vector_set vector_list full_pivot_column sparse_pivot_column heap_pivot_column bit_tree_pivot_column"
algos="standard twist chunk chunk_sequential spectral_sequence row swap exhaustive retrospective"

oneTimeSetUp() {
    set -u
}

parse() {
    tail -n +2 $1 | sort
}

test() {
    for file in $files
    do
        for repr in $reprs
        do
            for algo in $algos
            do
                tmpfile=$(mktemp -p $AUTOPKGTEST_TMP)
                ( set -x; /usr/bin/phat --ascii --${repr} --${algo} examples/${file}.dat ${tmpfile} )
                assertEquals "Non-zero return code." 0 $?
                [ "$(parse debian/tests/reference/${file}.txt)" == "$(parse $tmpfile)" ]
                assertEquals "Wrong result." 0 $?
            done
        done
    done
}

. shunit2
